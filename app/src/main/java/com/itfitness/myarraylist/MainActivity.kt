package com.itfitness.myarraylist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var arrayList = MyArrayList()
        for(i in 0 until 20){
            arrayList.add(i)
        }
        Log.e("移除前的大小","${arrayList.size()}")
        //移除掉一个值
        arrayList.remove(10)
        Log.e("移除后的大小","${arrayList.size()}")
        for (i in 0 until arrayList.size()){
            val element = arrayList.get(i)
            Log.e("获取的值","$element")
        }
    }
}
