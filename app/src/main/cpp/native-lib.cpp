#include <jni.h>
#include <string>
#include "ArrayList.cpp"
#include <android/log.h>
extern "C" JNIEXPORT jlong JNICALL
Java_com_itfitness_myarraylist_MyArrayList_createArrayList(
        JNIEnv* env,
        jobject jobj) {
    //这里是仿照OpenCv的Android依赖的源码实现的，将创建的ArrayList指针以long类型返回，等调用Native方法的时候再转成指针
    ArrayList<int>* arrlist = new ArrayList<int>();
    long addr = (long)arrlist;
    return addr;
}

extern "C" JNIEXPORT void JNICALL
Java_com_itfitness_myarraylist_MyArrayList_add(
        JNIEnv* env,
        jobject jobj,
        jlong addr,
        jint element) {
    ArrayList<int>* arrlist = (ArrayList<int>*)addr;
    arrlist->add(element);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_itfitness_myarraylist_MyArrayList_get(
        JNIEnv* env,
        jobject jobj,
        jlong addr,
        jint index) {
    ArrayList<int>* arrlist = (ArrayList<int>*)addr;
    return arrlist->get(index);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_itfitness_myarraylist_MyArrayList_size(
        JNIEnv* env,
        jobject jobj,
        jlong addr) {
    ArrayList<int>* arrlist = (ArrayList<int>*)addr;
    return arrlist->size();
}

extern "C" JNIEXPORT jint JNICALL
Java_com_itfitness_myarraylist_MyArrayList_remove(
        JNIEnv* env,
        jobject jobj,
        jlong addr,
        jint index) {
    ArrayList<int>* arrlist = (ArrayList<int>*)addr;
    return arrlist->remove(index);
}

/**
 * 释放空间
 */
extern "C" JNIEXPORT void JNICALL
Java_com_itfitness_myarraylist_MyArrayList_release(
        JNIEnv* env,
        jobject jobj,
        jlong addr) {
    ArrayList<int>* arrlist = (ArrayList<int>*)addr;
    delete(arrlist);
}
